
let mut files: Vec<DirEntry> = Vec::new();
let mut directories: Vec<DirEntry> = Vec::new();

if let Some(exists) = seen.get(filename.as_ref()) {
    exists.push(checked);
}
seen.insert(, checked);
if checked.file_type().is_dir() {
    directories.push(checked);
} else if checked.file_type().is_file() {
    files.push(checked);
} else {
    eprintln!(
        "unknown type {:?} for DirEntry {:?}",
        checked.file_type(),
        checked
    );
}
println!(
    "seen {} files in {} directories.",
    files.len(),
    directories.len()
);

fn collect_paths_strings(dir: &str) -> Result<HashSet<String>, std::io::Error> {
    let mut paths = HashSet::new();

    for entry in fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();
        if let Some(path_str) = path.to_str() {
            paths.insert(String::from(path_str));
        }
    }
    Ok(paths)
}