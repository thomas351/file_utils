use clap::Parser;
use env_logger::{Builder, TimestampPrecision};
use log::{debug, error, info, trace};
use sha2::{Digest, Sha256};
use std::{
    cmp::Ordering,
    collections::{HashMap, HashSet},
    env,
    fs::File,
    io::{stdin, stdout, Read, Write},
    path::PathBuf,
};
use strum_macros::Display;
use termios::{tcsetattr, Termios, ICANON, TCSANOW};
use walkdir::{DirEntry, WalkDir};

#[derive(Parser)]
enum Mode {
    #[clap(alias = "d")]
    DuplicateFilenames(TargetPath),
    #[clap(alias = "c")]
    CopyStructure(StructureFilesTarget),
    ReadChar,
}

#[derive(clap::Args)]
struct TargetPath {
    target: String,
}
#[derive(clap::Args, Debug)]
struct StructureFilesTarget {
    structure_source: String,
    file_source: String,
    target: String,
    #[clap(default_value = "1")]
    min_folder_depth: u16,
}

fn main() {
    if env::var("RUST_LOG").is_err() {
        env::set_var("RUST_LOG", "debug");
    }
    if env::var("RUST_LOG_STYLE").is_err() {
        env::set_var("RUST_LOG_STYLE", "always");
    }
    Builder::from_default_env()
        .format(|buf, record| writeln!(buf, "# {}", record.args()))
        .init();

    match Mode::parse() {
        Mode::DuplicateFilenames(args) => {
            print_duplicates_scan_result(duplicate_names(&args.target))
        }
        Mode::CopyStructure(args) => copy_structure(args),
        Mode::ReadChar => read_char(),
    }
}

fn read_char() {
    let stdin_fd = 0;
    let termios = Termios::from_fd(stdin_fd).unwrap();
    let mut new_termios = termios;
    new_termios.c_lflag &= !(ICANON);
    tcsetattr(stdin_fd, TCSANOW, &new_termios).unwrap();

    let stdout = stdout();
    let mut reader = stdin();
    let mut buffer = [0; 1];
    print!("Hit a key! ");
    stdout.lock().flush().unwrap();
    reader.read_exact(&mut buffer).unwrap();
    println!("\nYou have hit: {:?}", buffer);

    tcsetattr(stdin_fd, TCSANOW, &termios).unwrap();
}

fn compare_1type_2dir_depth_3path(
    a: &(UniqueNameKey, DirEntry),
    b: &(UniqueNameKey, DirEntry),
) -> Ordering {
    let cmp_type = a.0.path_type.cmp(&b.0.path_type);
    if !cmp_type.is_eq() {
        return cmp_type;
    }
    if a.0.path_type == PathType::Directory {
        let cmp_depth = a.0.depth.cmp(&b.0.depth);
        if !cmp_depth.is_eq() {
            return cmp_depth;
        }
    }
    a.1.path()
        .to_string_lossy()
        .cmp(&b.1.path().to_string_lossy())
}

fn copy_structure(args: StructureFilesTarget) {
    info!(
        "welcome to the directory tree structure copy tool! working with args: {:?}",
        args
    );
    let structure_source_id = uniquely_identify_all_children(&args.structure_source);
    let mut file_source_id = uniquely_identify_all_children(&args.file_source);

    let mut moved_paths: HashSet<PathBuf> = HashSet::new();

    let mut file_source_unique_names: Vec<(UniqueNameKey, DirEntry)> =
        file_source_id.unique_names.drain().collect();
    file_source_unique_names.sort_by(compare_1type_2dir_depth_3path);
    'outer: for (key, entry) in file_source_unique_names {
        if key.path_type == PathType::Directory && key.depth < args.min_folder_depth {
            debug!(
                "skipping unique directory name in favor of deeper structure ({}<{}): {:?}",
                key.depth, args.min_folder_depth, key
            );
            continue;
        }
        let mut ancestor = entry.path();
        for depth in (args.min_folder_depth..=key.depth).rev() {
            if moved_paths.contains(ancestor) {
                trace!("skip processing path {:?} because it's ancestor @ depth={depth} has already been moved", entry);
                continue 'outer;
            }
            ancestor = ancestor.parent().unwrap();
        }

        if let Some(structure_src) = structure_source_id.unique_names.get(&key) {
            move_entry(&args, structure_src, entry, &mut moved_paths);
            continue;
        }
        if key.path_type != PathType::File {
            info!("Name is not unique (or doesn't exist) in structure_source path, and it isn't a file. Therefore size and hash matching are not applicable. This entry will remain in file_source path: {:?}: {:?}", key, entry);
            continue;
        }
        let sized_key = (key.name, entry.metadata().unwrap().len());
        if let Some(structure_src) = structure_source_id.uniquely_sized.get(&sized_key) {
            move_entry(&args, structure_src, entry, &mut moved_paths);
            continue;
        }
        let hash_key = (sized_key.0, sized_key.1, calculate_sha256(&entry).unwrap());
        if let Some(structure_src) = structure_source_id.unique_hashes.get(&hash_key) {
            move_entry(&args, structure_src, entry, &mut moved_paths);
            continue;
        }
        if let Some(duplicates) = structure_source_id.duplicated_hashes.get(&hash_key) {
            info!("file has duplicates with same hash on ")
        }
        // TO DO: output info wether this file is non existent in structure_src or not unique there.
    }
    // TO DO: process uniquely_sized and unique_hashes from file_source.
}

fn move_entry(
    args: &StructureFilesTarget,
    structure_src: &DirEntry,
    entry: DirEntry,
    moved: &mut HashSet<PathBuf>,
) {
    println!(
        "mv \"{}{}\" \"{}{}/\"",
        args.file_source,
        entry.path().to_string_lossy().trim_start_matches('.'),
        args.target,
        structure_src
            .path()
            .parent()
            .unwrap()
            .to_string_lossy()
            .trim_start_matches('.')
    );
    stdout().flush().expect("Failed to flush stdout");
    moved.insert(entry.into_path());
}

fn calculate_sha256(target: &DirEntry) -> Result<[u8; 32], std::io::Error> {
    let mut file = File::open(target.path())?;
    let mut hash = Sha256::new();

    // Read the file chunk by chunk
    let mut buffer = [0; 1024];
    loop {
        let bytes_read = file.read(&mut buffer)?;
        if bytes_read == 0 {
            break;
        }
        hash.update(&buffer[..bytes_read]);
    }

    // Get the resulting hash
    let result = hash.finalize();
    // warn!("calculated sha256: {:?}", result);
    Ok(result.into())
}

struct IdentityScanResult {
    unique_names: HashMap<UniqueNameKey, DirEntry>,
    uniquely_sized: HashMap<(String, u64), DirEntry>,
    unique_hashes: HashMap<(String, u64, [u8; 32]), DirEntry>,
    duplicated_hashes: HashMap<(String, u64, [u8; 32]), Vec<DirEntry>>,
    duplicate_non_files: HashMap<UniqueNameKey, Vec<DirEntry>>,
}

fn uniquely_identify_all_children(root: &str) -> IdentityScanResult {
    let duplicate_name_scan = duplicate_names(root);
    let mut duplicate_non_files: HashMap<UniqueNameKey, Vec<DirEntry>> = HashMap::new();
    let mut uniquely_sized: HashMap<(String, u64), DirEntry> = HashMap::new();
    let mut duplicated_sizes: HashMap<(String, u64), Vec<DirEntry>> = HashMap::new();
    for (key, paths) in duplicate_name_scan.duplicates {
        if key.path_type != PathType::File {
            duplicate_non_files.insert(key, paths);
            continue;
        }
        let mut key_pattern = (key.name, 0_u64);
        for path in paths {
            match path.metadata() {
                Ok(metadata) => {
                    key_pattern.1 = metadata.len();
                    if let Some(multi) = duplicated_sizes.get_mut(&key_pattern) {
                        multi.push(path);
                    } else if let Some((key, duplicate)) = uniquely_sized.remove_entry(&key_pattern)
                    {
                        let multi = vec![duplicate, path];
                        duplicated_sizes.insert(key, multi);
                    } else {
                        uniquely_sized.insert(key_pattern.clone(), path);
                    }
                }
                Err(error) => error!(
                    "couldn't read metadata from filesystem for {:?} - error = {error}",
                    path
                ),
            }
        }
    }
    let mut unique_hashes: HashMap<(String, u64, [u8; 32]), DirEntry> = HashMap::new();
    let mut duplicated_hashes: HashMap<(String, u64, [u8; 32]), Vec<DirEntry>> = HashMap::new();
    for ((name, size), paths) in duplicated_sizes {
        let mut key_pattern = (name, size, [0_u8; 32]);
        for path in paths {
            match calculate_sha256(&path) {
                Ok(hash) => {
                    key_pattern.2 = hash;
                    if let Some(multi) = duplicated_hashes.get_mut(&key_pattern) {
                        multi.push(path);
                    } else if let Some((key, duplicate)) = unique_hashes.remove_entry(&key_pattern)
                    {
                        let multi = vec![duplicate, path];
                        duplicated_hashes.insert(key, multi);
                    } else {
                        unique_hashes.insert(key_pattern.clone(), path);
                    }
                }
                Err(error) => error!(
                    "couldn't calculate sha256 hash for {:?}, error={error}",
                    path
                ),
            }
        }
    }
    IdentityScanResult {
        unique_names: duplicate_name_scan.uniques,
        uniquely_sized,
        unique_hashes,
        duplicated_hashes,
        duplicate_non_files,
    }
}

struct DuplicateNameScanResult {
    uniques: HashMap<UniqueNameKey, DirEntry>,
    duplicates: HashMap<UniqueNameKey, Vec<DirEntry>>,
}

fn print_duplicates_scan_result(result: DuplicateNameScanResult) {
    info!(
        "found {} unique names, and the following {} non-unique names:",
        result.uniques.len(),
        result.duplicates.len()
    );

    let mut keys: Vec<&UniqueNameKey> = result.duplicates.keys().collect();
    keys.sort();
    for key in keys {
        println!("\n{} ({}):", key.name, key.path_type);
        for entry in result.duplicates.get(key).unwrap() {
            println!("{:?}", entry.path());
        }
    }
}

#[derive(PartialEq, Eq, Hash, PartialOrd, Ord, Debug, Display)]
enum PathType {
    Directory,
    File,
    Symlink,
    Other,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
struct UniqueNameKey {
    path_type: PathType,
    depth: u16,
    name: String,
}

fn duplicate_names(root: &str) -> DuplicateNameScanResult {
    let mut uniques: HashMap<UniqueNameKey, DirEntry> = HashMap::new();
    let mut duplicates: HashMap<UniqueNameKey, Vec<DirEntry>> = HashMap::new();
    for entry in WalkDir::new(root) {
        match entry {
            Ok(checked) => {
                let name = checked.file_name().to_string_lossy().to_string();
                let key = UniqueNameKey {
                    path_type: dir_entry_type(&checked),
                    depth: checked.depth().try_into().unwrap(),
                    name,
                };
                if let Some(multi) = duplicates.get_mut(&key) {
                    multi.push(checked);
                } else if let Some(duplicate) = uniques.remove(&key) {
                    let multi = vec![duplicate, checked];
                    duplicates.insert(key, multi);
                } else {
                    uniques.insert(key, checked);
                }
            }
            Err(error) => error!("duplicate_filenames scan error: {error}"),
        }
    }
    DuplicateNameScanResult {
        uniques,
        duplicates,
    }
}

fn dir_entry_type(entry: &DirEntry) -> PathType {
    if entry.path_is_symlink() {
        PathType::Symlink
    } else if entry.file_type().is_dir() {
        PathType::Directory
    } else if entry.file_type().is_file() {
        PathType::File
    } else {
        PathType::Other
    }
}
