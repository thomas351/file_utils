use clap::derive::Clap;
use clap::{App, Arg, ArgMatches};

#[derive(Parser, Debug)]
enum Mode {
    #[clap(alias = "1")]
    Mode1(String),

    #[clap(alias = "2")]
    Mode2(Vec<String>),
}

impl Mode {
    fn validate_mode2_args(args: &[String]) -> clap::Result<()> {
        if args.len() != 2 {
            Err(clap::Error::value_validation_auto(format!(
                "Expected 2 arguments, got {}",
                args.len()
            )))
        } else {
            Ok(())
        }
    }
}

impl<'a> FromArgMatches<'a> for Mode {
    fn from_arg_matches(matches: &ArgMatches<'a>) -> Self {
        match matches.subcommand() {
            ("1", Some(sub_m)) => Mode::Mode1(String::from(sub_m.value_of("MODE1").unwrap())),
            ("2", Some(sub_m)) => {
                let mode2_args = sub_m
                    .values_of_lossy("MODE2")
                    .expect("Expected MODE2 arguments");

                Mode::validate_mode2_args(&mode2_args)
                    .unwrap_or_else(|e| {
                        clap::Error::with_description(
                            &e.to_string(),
                            clap::ErrorKind::ValueValidation,
                        )
                    })
                    .expect("Invalid arguments for mode2");

                Mode::Mode2(mode2_args)
            }
            _ => unreachable!(),
        }
    }
}

#[derive(Parser, Debug)]
struct MyCli {
    mode: Mode,
}

fn main() {
    let my_cli = MyCli::parse();

    match my_cli.mode {
        Mode::Mode1(path) => {
            println!("Running in mode 1 with path: {}", path);
        }
        Mode::Mode2(paths) => {
            println!("Running in mode 2 with paths: {:?}", paths);
        }
    }
}
