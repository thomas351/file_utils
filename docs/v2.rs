use clap::Parser;
use env_logger::{Builder, TimestampPrecision};
use log::{debug, error, info};
use sha2::{Digest, Sha256};
use std::{
    borrow::Cow,
    collections::{HashMap, HashSet},
    env,
    fs::File,
    io::{stdin, stdout, Read, Write},
};
use strum_macros::Display;
use termios::{tcsetattr, Termios, ICANON, TCSANOW};
use walkdir::{DirEntry, WalkDir};

#[derive(Parser)]
enum Mode {
    #[clap(alias = "d")]
    DuplicateFilenames(TargetPath),
    #[clap(alias = "c")]
    CopyStructure(StructureFilesTarget),
    ReadChar,
}

#[derive(clap::Args)]
struct TargetPath {
    target: String,
}
#[derive(clap::Args, Debug)]
struct StructureFilesTarget {
    structure_source: String,
    file_source: String,
    target: String,
    #[clap(default_value = "1")]
    min_folder_depth: u16,
}

fn main() {
    if env::var("RUST_LOG").is_err() {
        env::set_var("RUST_LOG", "info");
    }
    Builder::from_default_env()
        .format_timestamp(Some(TimestampPrecision::Millis))
        .init();

    match Mode::parse() {
        Mode::DuplicateFilenames(args) => {
            print_duplicates_scan_result(duplicate_names(&args.target))
        }
        Mode::CopyStructure(args) => copy_structure(args),
        Mode::ReadChar => read_char(),
    }
}

fn read_char() {
    let stdin_fd = 0;
    let termios = Termios::from_fd(stdin_fd).unwrap();
    let mut new_termios = termios;
    new_termios.c_lflag &= !(ICANON);
    tcsetattr(stdin_fd, TCSANOW, &new_termios).unwrap();

    let stdout = stdout();
    let mut reader = stdin();
    let mut buffer = [0; 1];
    print!("Hit a key! ");
    stdout.lock().flush().unwrap();
    reader.read_exact(&mut buffer).unwrap();
    println!("\nYou have hit: {:?}", buffer);

    tcsetattr(stdin_fd, TCSANOW, &termios).unwrap();
}

fn copy_structure(args: StructureFilesTarget) {
    info!(
        "welcome to the directory tree structure copy tool! working with args: {:?}",
        args
    );
    let structure_source_id = uniquely_identify_all_children(&args.structure_source);
    let mut file_source_id = uniquely_identify_all_children(&args.file_source);

    let mut file_source_unique_names: Vec<(UniqueNameKey, DirEntry)> =
        file_source_id.unique_names.drain().collect();
    file_source_unique_names.sort();

    let mut keys: Vec<&UniqueNameKey> = file_source_id.unique_names.keys().collect();
    let mut moved_paths: HashSet<Cow<'_, str>> = HashSet::new();
    // sort keys, to handle folders first, and each type sorted by depth and each depth by alphabet. predictable order.
    keys.sort();
    // keys.remove(0); // always skip the root folder
    'key: for key in keys {
        if key.path_type == PathType::Directory && key.depth < args.min_folder_depth {
            debug!(
                "skipping unique directory name in favor of deeper structure ({}<{}): {:?}",
                key.depth, args.min_folder_depth, key
            );
            continue 'key;
        }
        let mv_src = file_source_id.unique_names.get(key).unwrap();

        let mut ancestor = mv_src.path();
        for depth in (args.min_folder_depth..=key.depth).rev() {
            if moved_paths.contains(&ancestor.to_string_lossy()) {
                debug!("skip processing path {:?} because it's ancestor @ depth={depth} has already been moved", mv_src);
                continue 'key;
            }
            ancestor = ancestor.parent().unwrap();
        }

        if let Some(structure) = structure_source_id.unique_names.get(&key) {
            let moved_path = mv_src.path().to_string_lossy();
            println!(
                "mv \"{}{}\" \"{}{}/\"",
                args.file_source,
                moved_path.trim_start_matches('.'),
                args.target,
                structure
                    .path()
                    .parent()
                    .unwrap()
                    .to_string_lossy()
                    .trim_start_matches('.')
            );
            moved_paths.insert(moved_path);
            continue 'key;
        }
        if key.path_type != PathType::File {
            debug!("name is not unique (or doesn't exist) in structure_source path, and it isn't a file, therefore size and hash matching are not applicable. skipping {:?}: {:?}", key,mv_src);
            continue 'key;
        }
        let sized_key = (key.name.clone(), mv_src.metadata().unwrap().len());
        if let Some(structure) = structure_source_id.uniquely_sized.get(&sized_key) {}
    }
}

fn calculate_sha256(target: &DirEntry) -> Result<[u8; 32], std::io::Error> {
    let mut file = File::open(target.path())?;
    let mut hash = Sha256::new();

    // Read the file chunk by chunk
    let mut buffer = [0; 1024];
    loop {
        let bytes_read = file.read(&mut buffer)?;
        if bytes_read == 0 {
            break;
        }
        hash.update(&buffer[..bytes_read]);
    }

    // Get the resulting hash
    let result = hash.finalize();
    // warn!("calculated sha256: {:?}", result);
    Ok(result.into())
}

struct IdentityScanResult {
    unique_names: HashMap<UniqueNameKey, DirEntry>,
    uniquely_sized: HashMap<(String, u64), DirEntry>,
    unique_hashes: HashMap<(String, u64, [u8; 32]), DirEntry>,
    duplicated_hashes: HashMap<(String, u64, [u8; 32]), Vec<DirEntry>>,
    duplicate_non_files: HashMap<UniqueNameKey, Vec<DirEntry>>,
}

fn uniquely_identify_all_children(root: &str) -> IdentityScanResult {
    let duplicate_name_scan = duplicate_names(root);
    let mut duplicate_non_files: HashMap<UniqueNameKey, Vec<DirEntry>> = HashMap::new();
    let mut uniquely_sized: HashMap<(String, u64), DirEntry> = HashMap::new();
    let mut duplicated_sizes: HashMap<(String, u64), Vec<DirEntry>> = HashMap::new();
    for (key, paths) in duplicate_name_scan.duplicates {
        if key.path_type != PathType::File {
            duplicate_non_files.insert(key, paths);
            continue;
        }
        let mut key_pattern = (key.name, 0_u64);
        for path in paths {
            match path.metadata() {
                Ok(metadata) => {
                    key_pattern.1 = metadata.len();
                    if let Some(multi) = duplicated_sizes.get_mut(&key_pattern) {
                        multi.push(path);
                    } else if let Some((key, duplicate)) = uniquely_sized.remove_entry(&key_pattern)
                    {
                        let multi = vec![duplicate, path];
                        duplicated_sizes.insert(key, multi);
                    } else {
                        uniquely_sized.insert(key_pattern.clone(), path);
                    }
                }
                Err(error) => error!(
                    "couldn't read metadata from filesystem for {:?} - error = {error}",
                    path
                ),
            }
        }
    }
    let mut unique_hashes: HashMap<(String, u64, [u8; 32]), DirEntry> = HashMap::new();
    let mut duplicated_hashes: HashMap<(String, u64, [u8; 32]), Vec<DirEntry>> = HashMap::new();
    for ((name, size), paths) in duplicated_sizes {
        let mut key_pattern = (name, size, [0_u8; 32]);
        for path in paths {
            match calculate_sha256(&path) {
                Ok(hash) => {
                    key_pattern.2 = hash;
                    if let Some(multi) = duplicated_hashes.get_mut(&key_pattern) {
                        multi.push(path);
                    } else if let Some((key, duplicate)) = unique_hashes.remove_entry(&key_pattern)
                    {
                        let multi = vec![duplicate, path];
                        duplicated_hashes.insert(key, multi);
                    } else {
                        unique_hashes.insert(key_pattern.clone(), path);
                    }
                }
                Err(error) => error!(
                    "couldn't calculate sha256 hash for {:?}, error={error}",
                    path
                ),
            }
        }
    }
    IdentityScanResult {
        unique_names: duplicate_name_scan.uniques,
        uniquely_sized,
        unique_hashes,
        duplicated_hashes,
        duplicate_non_files,
    }
}

struct DuplicateNameScanResult {
    uniques: HashMap<UniqueNameKey, DirEntry>,
    duplicates: HashMap<UniqueNameKey, Vec<DirEntry>>,
}

fn print_duplicates_scan_result(result: DuplicateNameScanResult) {
    info!(
        "found {} unique names, and the following {} non-unique names:",
        result.uniques.len(),
        result.duplicates.len()
    );

    let mut keys: Vec<&UniqueNameKey> = result.duplicates.keys().collect();
    keys.sort();
    for key in keys {
        println!("\n{} ({}):", key.name, key.path_type);
        for entry in result.duplicates.get(key).unwrap() {
            println!("{:?}", entry.path());
        }
    }
}

#[derive(PartialEq, Eq, Hash, PartialOrd, Ord, Debug, Display)]
enum PathType {
    Directory,
    File,
    Symlink,
    Other,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
struct UniqueNameKey {
    path_type: PathType,
    depth: u16,
    name: String,
}

fn duplicate_names(root: &str) -> DuplicateNameScanResult {
    let mut uniques: HashMap<UniqueNameKey, DirEntry> = HashMap::new();
    let mut duplicates: HashMap<UniqueNameKey, Vec<DirEntry>> = HashMap::new();
    for entry in WalkDir::new(root) {
        match entry {
            Ok(checked) => {
                let name = checked.file_name().to_string_lossy().to_string();
                let key = UniqueNameKey {
                    path_type: dir_entry_type(&checked),
                    depth: checked.depth().try_into().unwrap(),
                    name,
                };
                if let Some(multi) = duplicates.get_mut(&key) {
                    multi.push(checked);
                } else if let Some(duplicate) = uniques.remove(&key) {
                    let multi = vec![duplicate, checked];
                    duplicates.insert(key, multi);
                } else {
                    uniques.insert(key, checked);
                }
            }
            Err(error) => error!("duplicate_filenames scan error: {error}"),
        }
    }
    DuplicateNameScanResult {
        uniques,
        duplicates,
    }
}

fn dir_entry_type(entry: &DirEntry) -> PathType {
    if entry.path_is_symlink() {
        PathType::Symlink
    } else if entry.file_type().is_dir() {
        PathType::Directory
    } else if entry.file_type().is_file() {
        PathType::File
    } else {
        PathType::Other
    }
}
