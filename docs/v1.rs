use std::collections::HashMap;

use clap::{Parser, ValueEnum};
use walkdir::{DirEntry, WalkDir};

#[derive(Parser, Debug)]
#[command(author, version, verbatim_doc_comment)]
// This program contains an arbitrary collections of file organization related tools I needed.
struct Cli {
    #[arg(short, long)]
    target: String,

    #[arg(short, long, value_enum, default_value_t = Mode::DuplicateFilenames)]
    mode: Mode,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ValueEnum, Debug)]
enum Mode {
    DuplicateFilenames,
}

fn main() {
    let args = Cli::parse();
    println!("target specified: {}", args.target);
    match args.mode {
        Mode::DuplicateFilenames => {
            duplicate_filenames(args.target);
        }
    }
}

fn duplicate_filenames(path: String) -> HashMap<String, Vec<DirEntry>> {
    let mut uniques: HashMap<String, DirEntry> = HashMap::new();
    let mut duplicates: HashMap<String, Vec<DirEntry>> = HashMap::new();
    for entry in WalkDir::new(path) {
        match entry {
            Ok(checked) => {
                let filename = checked.file_name().to_string_lossy().to_string();
                if let Some(multi) = duplicates.get_mut(&filename) {
                    multi.push(checked);
                } else if let Some(duplicate) = uniques.remove(&filename) {
                    let multi = vec![duplicate, checked];
                    duplicates.insert(filename, multi);
                } else {
                    uniques.insert(filename, checked);
                }
            }
            Err(error) => eprintln!("error: {error}"),
        }
    }
    println!(
        "found {} unique names, and the following {} non-unique names:",
        uniques.len(),
        duplicates.len()
    );

    let mut names: Vec<&String> = duplicates.keys().collect();
    names.sort();
    for name in names {
        println!("\n{name}:");
        for entry in duplicates.get(name).unwrap() {
            println!("{:?} ({})", entry.path(), dir_entry_type(entry));
        }
    }
    duplicates
}

fn dir_entry_type(entry: &DirEntry) -> &str {
    if entry.path_is_symlink() {
        "symlink"
    } else if entry.file_type().is_dir() {
        "dir"
    } else if entry.file_type().is_file() {
        "file"
    } else {
        "other"
    }
}
