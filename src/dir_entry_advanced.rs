use once_cell::sync::OnceCell;
use sha2::{Digest, Sha256};
use std::{
    borrow::Cow,
    fs::{self, File},
    hash::{Hash, Hasher},
    io::Read,
    path::Path,
    time::SystemTime,
};
use strum_macros::Display;
use walkdir::DirEntry;

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
pub enum CompareMode {
    Name,
    Size,
    Time,
    Content,
}

#[derive(PartialEq, Eq, Hash, PartialOrd, Ord, Debug, Display, Clone, Copy)]
pub enum EntryType {
    Directory,
    File,
    Symlink,
    Other,
}

#[derive(Debug)]
pub struct DirEntryAdvanced {
    pub entry: DirEntry,
    pub entry_type: EntryType,
    pub compare_mode: CompareMode,
    metadata: OnceCell<fs::Metadata>,
    sha256: OnceCell<[u8; 32]>,
}

impl PartialEq for DirEntryAdvanced {
    fn eq(&self, other: &Self) -> bool {
        match self.compare_mode {
            CompareMode::Name => self.entry_type == other.entry_type && self.name() == other.name(),
            CompareMode::Size => self.name() == other.name() && self.size() == other.size(),
            CompareMode::Time => {
                self.name() == other.name()
                    && self.size() == other.size()
                    && self.mod_time() == other.mod_time()
            }
            CompareMode::Content => self.sha256() == other.sha256(),
        }
    }
}
impl Eq for DirEntryAdvanced {}
impl PartialOrd for DirEntryAdvanced {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
impl Ord for DirEntryAdvanced {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.compare_mode {
            CompareMode::Name => {
                match self.entry_type.cmp(&other.entry_type) {
                    std::cmp::Ordering::Equal => {}
                    ordering => {
                        return ordering;
                    }
                }
                match self.depth().cmp(&other.depth()) {
                    std::cmp::Ordering::Equal => {}
                    ordering => {
                        return ordering;
                    }
                }
                self.name().cmp(&other.name())
            }
            CompareMode::Size => {
                match self.size().cmp(&other.size()) {
                    std::cmp::Ordering::Equal => {}
                    ordering => {
                        return ordering;
                    }
                }
                self.name().cmp(&other.name())
            }
            CompareMode::Time => {
                match self.size().cmp(&other.size()) {
                    std::cmp::Ordering::Equal => {}
                    ordering => {
                        return ordering;
                    }
                }
                match self.mod_time().cmp(&other.mod_time()) {
                    std::cmp::Ordering::Equal => {}
                    ordering => {
                        return ordering;
                    }
                }
                self.name().cmp(&other.name())
            }
            CompareMode::Content => self.sha256().cmp(other.sha256()),
        }
    }
}
impl Hash for DirEntryAdvanced {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self.compare_mode {
            CompareMode::Name => {
                self.entry_type.hash(state);
                self.name().hash(state);
            }
            CompareMode::Size => {
                self.size().hash(state);
                self.name().hash(state);
            }
            CompareMode::Time => {
                self.size().hash(state);
                self.name().hash(state);
                self.mod_time().hash(state);
            }
            CompareMode::Content => {
                self.sha256().hash(state);
            }
        }
    }
}

impl DirEntryAdvanced {
    pub fn new(entry: DirEntry, compare_mode: CompareMode) -> Self {
        DirEntryAdvanced {
            entry_type: dir_entry_type(&entry),
            entry,
            compare_mode,
            metadata: OnceCell::new(),
            sha256: OnceCell::new(),
        }
    }

    pub fn name(&self) -> Cow<str> {
        self.entry.file_name().to_string_lossy()
    }

    pub fn size(&self) -> u64 {
        self.metadata().len()
    }

    pub fn mod_time(&self) -> SystemTime {
        self.metadata()
            .modified()
            .or_else(|_error| self.metadata().created())
            .unwrap()
    }

    pub fn metadata(&self) -> &fs::Metadata {
        self.metadata.get_or_init(|| self.entry.metadata().unwrap())
    }

    pub fn sha256(&self) -> &[u8; 32] {
        self.sha256
            .get_or_init(|| calculate_sha256(&self.entry).unwrap())
    }

    pub fn depth(&self) -> u16 {
        self.entry.depth().try_into().unwrap()
    }

    pub fn path(&self) -> &Path {
        self.entry.path()
    }
}

fn calculate_sha256(target: &DirEntry) -> Result<[u8; 32], std::io::Error> {
    let mut file = File::open(target.path())?;
    let mut hasher = Sha256::new();

    // Read the file chunk by chunk
    let mut buffer = [0; 1024];
    loop {
        let bytes_read = file.read(&mut buffer)?;
        if bytes_read == 0 {
            break;
        }
        hasher.update(&buffer[..bytes_read]);
    }

    // Get the resulting hash
    let hash = hasher.finalize();
    // warn!("calculated sha256: {:?}", result);
    Ok(hash.into())
}

fn dir_entry_type(entry: &DirEntry) -> EntryType {
    if entry.path_is_symlink() {
        EntryType::Symlink
    } else if entry.file_type().is_dir() {
        EntryType::Directory
    } else if entry.file_type().is_file() {
        EntryType::File
    } else {
        EntryType::Other
    }
}
