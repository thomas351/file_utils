// combine multiple if let constructs with each other and/or boolean conditions
#![feature(let_chains)]

use chrono::Local;
use clap::Parser;
use dir_entry_advanced::{CompareMode, DirEntryAdvanced, EntryType};
use env_logger::Builder;
use log::{debug, error, info, trace};
use std::{
    borrow::Cow,
    collections::{HashMap, HashSet},
    env,
    io::{stdin, stdout, Read, Write},
    path::PathBuf,
    time::{Duration, Instant},
};
use termios::{tcsetattr, Termios, ICANON, TCSANOW};
use walkdir::WalkDir;

mod dir_entry_advanced;

#[derive(Parser)]
enum Mode {
    #[clap(alias = "d")]
    DuplicateFilenames(TargetPath),
    #[clap(alias = "c")]
    CopyStructure(StructureFilesCall),
    ReadChar,
}

#[derive(clap::Args)]
struct TargetPath {
    target: String,
}
#[derive(clap::Args, Debug)]
struct StructureFilesCall {
    structure_source: String,
    file_source: String,
    target: String,
    #[clap(default_value = "1")]
    min_folder_depth: u16,
}

fn main() {
    if env::var("RUST_LOG").is_err() {
        env::set_var("RUST_LOG", "debug");
    }
    // if env::var("RUST_LOG_STYLE").is_err() {
    //     env::set_var("RUST_LOG_STYLE", "always");
    // }
    Builder::from_default_env()
        // .write_style(env_logger::WriteStyle::Never)
        .format(|buf, record| {
            let timestamp = Local::now().format("%Y-%m-%d %H:%M:%S%.3f");
            writeln!(
                buf,
                "# {} {} - {}",
                timestamp,
                record.level(),
                record.args()
            )
        })
        .init();

    match Mode::parse() {
        Mode::DuplicateFilenames(args) => {
            print_name_scan_result(duplicate_names_scan(&args.target))
        }
        Mode::CopyStructure(args) => copy_structure(args),
        Mode::ReadChar => read_char(),
    }
}

fn read_char() {
    let stdin_fd = 0;
    let termios = Termios::from_fd(stdin_fd).unwrap();
    let mut new_termios = termios;
    new_termios.c_lflag &= !(ICANON);
    tcsetattr(stdin_fd, TCSANOW, &new_termios).unwrap();

    let stdout = stdout();
    let mut reader = stdin();
    let mut buffer = [0; 1];
    print!("Hit a key! ");
    stdout.lock().flush().unwrap();
    reader.read_exact(&mut buffer).unwrap();
    println!("\nYou have hit: {:?}", buffer);

    tcsetattr(stdin_fd, TCSANOW, &termios).unwrap();
}

fn handle_file_source_entry(
    mut entry: DirEntryAdvanced,
    structure_src_id: &IdentityScanResult,
    moved: &mut HashSet<PathBuf>,
    created: &mut HashSet<String>,
    args: &StructureFilesCall,
    mode: CompareMode,
) {
    if entry.entry_type == EntryType::Directory && entry.depth() < args.min_folder_depth {
        debug!(
            "skipping directory name in favor of deeper structure, because entry.depth < args.min_folder_depth ({}<{}): {:?}",
            entry.depth(),
            args.min_folder_depth,
            entry
        );
        return;
    }
    let mut ancestor = entry.path();
    for depth in (args.min_folder_depth..=entry.depth()).rev() {
        if moved.contains(ancestor) {
            trace!("skip processing path {:?} because it's ancestor @ depth={depth} has already been moved", entry);
            return;
        }
        ancestor = ancestor.parent().unwrap();
    }

    let mut skip_to_content_check = false;
    let skip_size = mode == CompareMode::Name;
    let skip_time = mode != CompareMode::Time;
    let skip_hash = mode != CompareMode::Content;

    entry.compare_mode = CompareMode::Name;
    if let Some(structure_src) = structure_src_id.unique_names.get(&entry) {
        // if !(skip_size || entry.size() == structure_src.size()) {
        //     warn!("skipping file_src entry, because it doesn't have unique name in file_src, but the matching unique name in structure_src differs in size! file_src.entry={:?}, structure_src.entry={:?}", entry, structure_src);
        // }
        if (skip_size || entry.size() == structure_src.size())
            && (skip_time || entry.mod_time() == structure_src.mod_time())
            && (skip_hash || entry.sha256() == structure_src.sha256())
        {
            move_entry(&args, structure_src, entry, moved, created);
            return;
        } else {
            debug!("file_src entry doesn't have a unique name, even though on structure_src that name is unique but differs in size or hash. We'll see if a different structure_src entry maybe has a matching hash. {:?}", entry);
            skip_to_content_check = true;
        }
    } else if entry.entry_type != EntryType::File {
        let key = (entry.path().to_string_lossy(), entry.entry_type);
        if let Some(duplicates) = structure_src_id.duplicate_non_files.get(&key) {
            info!("non-file does not have a unique name in structure_source. leaving it as is: {:?} - structure_source duplicates: {:?}", entry, duplicates);
        } else {
            info!("non-file does not exist in structure_source path. This entry will remain in file_source path: {:?}: {:?}", key, entry);
        }
        return;
    }
    if !skip_to_content_check {
        entry.compare_mode = CompareMode::Size;
        if let Some(structure_src) = structure_src_id.unique_sizes.get(&entry) {
            if (skip_time || entry.mod_time() == structure_src.mod_time())
                && (skip_hash || entry.sha256() == structure_src.sha256())
            {
                move_entry(&args, structure_src, entry, moved, created);
                return;
            } else {
                debug!("file_src entry doesn't have a unique name+size combination, even though on structure_src it does but with differing timestamp or content. We'll see if a different structure_src entry maybe has a matching hash. {:?}", entry);
                skip_to_content_check = true;
            }
        }
    }
    if !skip_to_content_check {
        entry.compare_mode = CompareMode::Time;
        if let Some(structure_src) = structure_src_id.unique_times.get(&entry) {
            if skip_hash || entry.sha256() == structure_src.sha256() {
                move_entry(&args, structure_src, entry, moved, created);
                return;
            } else {
                debug!("file_src entry doesn't have a unique name+size+timestamp combination, even though on structure_src it does but with differing content. We'll see if a different structure_src entry maybe has a matching hash. {:?}", entry);
            }
        }
    }

    entry.compare_mode = CompareMode::Content;
    if let Some(structure_src) = structure_src_id.unique_content.get(&entry) {
        move_entry(&args, structure_src, entry, moved, created);
    } else {
        let key = (entry.size(), *entry.sha256());
        if let Some(duplicates) = structure_src_id.duplicated_hashes.get(&key) {
            info!("file has multiple duplicates with same hash in structure source. since there is no unique place to put it, we'll leave it in the file_source. please either prepare your structure and file sources to prevent this or come up with an algorithm that can handle this case and implement it. {:?} - structure_source_duplicates: {:?}", entry, duplicates);
        } else {
            info!(
                "entry does not exist in structure_source, leaving in file_source: {:?}",
                entry
            );
        }
    }
}

fn copy_structure(args: StructureFilesCall) {
    info!(
        "welcome to the directory tree structure copy tool! working with args: {:?}",
        args
    );
    let structure_src_id = identity_scan(&args.structure_source);
    info!("structure_src identity scan complete!");
    let mut file_src_id = identity_scan(&args.file_source);
    info!("file_src identity scan complete!");

    let mut moved: HashSet<PathBuf> = HashSet::new();
    let mut created: HashSet<String> = HashSet::new();
    created.insert(args.target.clone());

    let mut file_src_unique_names: Vec<DirEntryAdvanced> =
        file_src_id.unique_names.drain().collect();
    file_src_unique_names.sort();
    info!("sorted file_src_unique_names, handling each entry in order now:");
    for entry in file_src_unique_names {
        handle_file_source_entry(
            entry,
            &structure_src_id,
            &mut moved,
            &mut created,
            &args,
            CompareMode::Name,
        );
    }

    let mut file_src_unique_sizes: Vec<DirEntryAdvanced> =
        file_src_id.unique_sizes.drain().collect();
    file_src_unique_sizes.sort();
    info!("sorted file_source_uniquely_sized, handling each entry in order now:");
    for entry in file_src_unique_sizes {
        handle_file_source_entry(
            entry,
            &structure_src_id,
            &mut moved,
            &mut created,
            &args,
            CompareMode::Size,
        );
    }

    let mut file_src_unique_times: Vec<DirEntryAdvanced> =
        file_src_id.unique_times.drain().collect();
    file_src_unique_times.sort();
    info!("sorted file_source_uniquely_timed, handling each entry in order now:");
    for entry in file_src_unique_times {
        handle_file_source_entry(
            entry,
            &structure_src_id,
            &mut moved,
            &mut created,
            &args,
            CompareMode::Time,
        );
    }

    let mut file_source_unique_hashes: Vec<DirEntryAdvanced> =
        file_src_id.unique_content.drain().collect();
    file_source_unique_hashes.sort();
    info!("sorted file_source_unique_hashes, handling each entry in order now:");
    for entry in file_source_unique_hashes {
        handle_file_source_entry(
            entry,
            &structure_src_id,
            &mut moved,
            &mut created,
            &args,
            CompareMode::Content,
        );
    }

    info!("directory tree structure copy tool finished!");
}

fn move_entry(
    args: &StructureFilesCall,
    structure_src: &DirEntryAdvanced,
    entry: DirEntryAdvanced,
    moved: &mut HashSet<PathBuf>,
    created: &mut HashSet<String>,
) {
    let parent = format!(
        "{}{}",
        args.target,
        structure_src
            .path()
            .parent()
            .unwrap()
            .to_string_lossy()
            .trim_start_matches('.')
    );
    let new_parent = !created.contains(&parent);
    if new_parent {
        println!("mkdir -p \"{parent}\"");
    }
    println!(
        "mv \"{}{}\" \"{}/{}\"",
        args.file_source,
        entry.path().to_string_lossy().trim_start_matches('.'),
        parent,
        structure_src.name(),
    );
    stdout().flush().expect("Failed to flush stdout");
    moved.insert(entry.entry.into_path());
    if new_parent {
        created.insert(parent);
    }
}

struct IdentityScanResult<'a> {
    unique_names: HashSet<DirEntryAdvanced>,
    unique_sizes: HashSet<DirEntryAdvanced>,
    unique_times: HashSet<DirEntryAdvanced>,
    unique_content: HashSet<DirEntryAdvanced>,
    duplicated_hashes: HashMap<(u64, [u8; 32]), Vec<DirEntryAdvanced>>,
    duplicate_non_files: HashMap<(Cow<'a, str>, EntryType), Vec<DirEntryAdvanced>>,
}

fn use_size(result: &mut IdentityScanResult, mut entry: DirEntryAdvanced) -> CompareMode {
    entry.compare_mode = CompareMode::Size;
    if let Some(conflict) = result.unique_sizes.take(&entry) {
        use_time(result, conflict);
        use_time(result, entry);
        CompareMode::Time
    } else {
        result.unique_sizes.insert(entry);
        CompareMode::Size
    }
}

fn use_time(result: &mut IdentityScanResult, mut entry: DirEntryAdvanced) -> CompareMode {
    entry.compare_mode = CompareMode::Time;
    if let Some(conflict) = result.unique_times.take(&entry) {
        use_content(result, conflict);
        use_content(result, entry);
        CompareMode::Content
    } else {
        result.unique_times.insert(entry);
        CompareMode::Time
    }
}

fn use_content(result: &mut IdentityScanResult, mut entry: DirEntryAdvanced) {
    entry.compare_mode = CompareMode::Content;
    let key = (entry.size(), entry.sha256().clone());
    if let Some(duplicates) = result.duplicated_hashes.get_mut(&key) {
        duplicates.push(entry);
    } else if let Some(duplicate) = result.unique_content.take(&entry) {
        let vec = vec![duplicate, entry];
        result.duplicated_hashes.insert(key, vec);
    } else {
        result.unique_content.insert(entry);
    }
}

fn identity_scan(root: &str) -> IdentityScanResult {
    let duplicate_name_scan = duplicate_names_scan(root);
    let mut result = IdentityScanResult {
        unique_names: duplicate_name_scan.uniques,
        unique_sizes: HashSet::new(),
        unique_times: HashSet::new(),
        unique_content: HashSet::new(),
        duplicated_hashes: HashMap::new(),
        duplicate_non_files: HashMap::new(),
    };
    info!("now trying to identify files with duplicate names by size and if inconclusive by the sha256 hash of their contents.");
    let mut last_status_time = Instant::now();
    let duplicate_names_count = duplicate_name_scan.duplicates.len();
    let mut scanned = 0_usize;
    let mut hashes_calculated = 0_usize;
    for ((name, entry_type), entries) in duplicate_name_scan.duplicates {
        if entry_type != EntryType::File {
            result
                .duplicate_non_files
                .insert((name, entry_type), entries);
            continue;
        };
        let mut mode = CompareMode::Size;
        let mut no_content = true;
        for entry in entries {
            match mode {
                CompareMode::Name => panic!("you're stupid."),
                CompareMode::Size => mode = use_size(&mut result, entry),
                CompareMode::Time => mode = use_time(&mut result, entry),
                CompareMode::Content => use_content(&mut result, entry),
            };

            if mode == CompareMode::Content {
                match no_content {
                    true => hashes_calculated += 2,
                    false => hashes_calculated += 1,
                }
                no_content = false;
            }

            let current_time = Instant::now();
            if current_time.duration_since(last_status_time) >= Duration::from_secs(1) {
                //  so far (some uniques might be moved to non-uniques if duplicates show up in the future)
                info!(
                    "identity-scan progress: {}/{duplicate_names_count} duplicate names done, {} uniquely-sized for now, {} hashes calculated",
                    scanned,
                    result.unique_sizes.len(),
                    hashes_calculated,
                );
                last_status_time = current_time;
            }
        }
        scanned += 1;
    }
    result
}

struct DuplicateNameScanResult<'a> {
    uniques: HashSet<DirEntryAdvanced>,
    duplicates: HashMap<(Cow<'a, str>, EntryType), Vec<DirEntryAdvanced>>,
}

fn print_name_scan_result(result: DuplicateNameScanResult) {
    let mut keys: Vec<&(Cow<str>, EntryType)> = result.duplicates.keys().collect();
    keys.sort();
    for key in keys {
        println!("\n{} ({}):", key.0, key.1);
        for entry in result.duplicates.get(key).unwrap() {
            println!("{:?}", entry.entry.path());
        }
    }
}

fn duplicate_names_scan(root: &str) -> DuplicateNameScanResult {
    info!("duplicate names scan started, root: {root}");
    let mut uniques: HashSet<DirEntryAdvanced> = HashSet::new();
    let mut duplicates: HashMap<(Cow<str>, EntryType), Vec<DirEntryAdvanced>> = HashMap::new();
    let mut last_status_time = Instant::now();
    for entry in WalkDir::new(root) {
        match entry {
            Ok(checked) => {
                let advanced = DirEntryAdvanced::new(checked, CompareMode::Name);
                let key = (
                    Cow::Owned(advanced.name().as_ref().to_string()),
                    advanced.entry_type,
                );
                if let Some(multi) = duplicates.get_mut(&key) {
                    multi.push(advanced);
                } else if let Some(duplicate) = uniques.take(&advanced) {
                    let multi = vec![duplicate, advanced];
                    duplicates.insert(key, multi);
                } else {
                    uniques.insert(advanced);
                }
            }
            Err(error) => error!("duplicate_names: WalkDir scan error: {error}"),
        }

        let current_time = Instant::now();
        if current_time.duration_since(last_status_time) >= Duration::from_secs(1) {
            //  so far (some uniques might be moved to non-uniques if duplicates show up in the future)
            info!(
                "name-scan progress: {} unique names, {} non-unique names",
                uniques.len(),
                duplicates.len()
            );
            last_status_time = current_time;
        }
    }

    info!(
        "scan completed. found {} unique names, and {} non-unique names.",
        uniques.len(),
        duplicates.len()
    );

    DuplicateNameScanResult {
        uniques,
        duplicates,
    }
}
