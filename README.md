# Ideas

* what to do about bad file I/O performance?
  * cache name_scans to disk between run's
  * cache calculated hashes to disk between run's
  * have code running on both sides of the ssh connection, like rsync
  * parallelize scans for cases where structure and file sources are on different disks / file-systems
  * before calculating a hash, check if the other side already has scanned a file with same name, size & mod-timestamp
